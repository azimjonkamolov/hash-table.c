// Name: Hash table.c
// Time: 13:10 05.10.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: hash table

#include<stdio.h>
#include<stdlib.h>

int ht[10], i, found=0, key;

void insert();
void del();
void search();
void display();

int main()
{
	int op;
	for(i=0;i<10;i++)
		ht[i]=-1;
	do
	{
		printf("\tMENU\n1.Insert\n2.Display\n3.Delete\n4.Search\n");
		printf("Enter your option:\n");
		scanf("%d", &op);
		switch(op)
		{
			case 1:
				insert();
				break;
			case 2:
				display();
				break;
			case 3:
				del();
				break;
			case 4:
				search();
				break;
			default:
				printf("Invalid number!\n");
				break;
		}
		printf("\n");
	}while(op!=5);

	
	return 0;
}

void insert()
{
	int val, f=0;
	printf("Enter the number:\n");
	scanf("%d", &val);
	key=(val%10)-1;
	if(ht[key]==-1)
		ht[key]=val;
	else
	{
		if(key<9)
		{
			for(i=key+1;key<10;i++)
			{
				if(ht[i]==-1)
				{
					ht[i]=val;
					break;	
				}
			}
		}
		for(i=0;i<key;i++)
		{
			if(ht[i]==-1)
			{
				ht[i]=val;
				break;
			}
		}
	}
}

void display()
{
	for(i=0;i<10;i++)
		printf("%d\t", ht[i]);
}

void search()
{
	int val, flag=0;
	printf("\nEnter the element to be searched:\n");
	scanf("%d", &val);
	key=(val%10)-1;
	if(ht[key]==val)
		flag=1;
	else
	{
		for(i=key+1;i<10;i++)
		{
			if(ht[i]==val)
			{
				flag=1;
				key=i;
				break;
			}
		}
	}
	if(flag==0)
	{
		for(i=0;i<key;i++)
		{
			if(ht[i]==val)
			{
				flag=1;
				key=1;
				break;
			}
		}
	}
	if(flag==1)
	{
		found=1;
		printf("\nThe item searched was found at position %d!\n", key+1);
	}
	else
	{
		key=-1;
		printf("\nThe item searched was not found in the hash table\n");
	}
}

void del()
{
	search();
	if(found==1)
	{
		if(key!=-1)
		{
			printf("The element deleted is %d\n", ht[key]);
			ht[key]=-1;
		}
	}
}













